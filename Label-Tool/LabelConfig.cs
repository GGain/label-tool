﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Label_Tool_master
{
    public partial class LabelConfig : Form
    {
        public string LabelListFileName { set; get; }
        public LabelConfig(string imgdir)
        {
            LabelListFileName = imgdir + "\\" + "obj.names";
            InitializeComponent();
            LoadLabelList();
        }
        public LabelConfig()
        {
            InitializeComponent();
            LoadLabelList();
        }
        

        public void LoadLabelList()
        {
            if (File.Exists(LabelListFileName))
            {
                string[] lines = File.ReadAllLines(LabelListFileName);
                listBox1.Items.AddRange(lines);
            }
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1 && listBox1.Items.Count != 0)
            {
                //remove the obj in the list
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                string[] text = new string[listBox1.Items.Count];
                int i = 0;
                foreach (string line in listBox1.Items)
                {
                    text[i] = line;
                    i++;
                }
                File.WriteAllLines(LabelListFileName, text);
               
            }
        }

        private void buttonAddLabel_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(textBoxNewLabel.Text))
            {

                if (listBox1.Items.Contains(textBoxNewLabel.Text)){
                    MessageBox.Show(string.Format("Label \"{0}\" is on the list!", textBoxNewLabel.Text));
                }
                else {
                    listBox1.Items.Add(textBoxNewLabel.Text);
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(LabelListFileName, true))
                    {
                        file.WriteLine(textBoxNewLabel.Text);
                    }
                }
                textBoxNewLabel.Text = string.Empty;
            }
        }
    }
}
