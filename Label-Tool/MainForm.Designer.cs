﻿ namespace Label_Tool_master
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonPrev = new System.Windows.Forms.Button();
            this.LabelImageDir = new System.Windows.Forms.Label();
            this.labelProgress = new System.Windows.Forms.Label();
            this.textBoxImgDir = new System.Windows.Forms.TextBox();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonDelAll = new System.Windows.Forms.Button();
            this.textBoxProcessNo = new System.Windows.Forms.TextBox();
            this.buttonOpenDialog = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelMousePosition = new System.Windows.Forms.Label();
            this.listBoxBoundary = new System.Windows.Forms.ListBox();
            this.labelBoundary = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonVideo = new System.Windows.Forms.Button();
            this.buttonCapture = new System.Windows.Forms.Button();
            this.buttonDeleteImage = new System.Windows.Forms.Button();
            this.checkBoxStereoCamera = new System.Windows.Forms.CheckBox();
            this.checkBoxLeft = new System.Windows.Forms.CheckBox();
            this.checkBoxRight = new System.Windows.Forms.CheckBox();
            this.buttonGenerateList = new System.Windows.Forms.Button();
            this.buttonStartTraining = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.comboBoxListLabel = new System.Windows.Forms.ComboBox();
            this.checkBoxTestImage = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonPrev
            // 
            this.buttonPrev.Location = new System.Drawing.Point(385, 454);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(75, 23);
            this.buttonPrev.TabIndex = 0;
            this.buttonPrev.Text = "<< Previous";
            this.buttonPrev.UseVisualStyleBackColor = true;
            this.buttonPrev.Click += new System.EventHandler(this.ButtonPrev_Click);
            // 
            // LabelImageDir
            // 
            this.LabelImageDir.AutoSize = true;
            this.LabelImageDir.Location = new System.Drawing.Point(12, 10);
            this.LabelImageDir.Name = "LabelImageDir";
            this.LabelImageDir.Size = new System.Drawing.Size(55, 13);
            this.LabelImageDir.TabIndex = 1;
            this.LabelImageDir.Text = "Image Dir:";
            // 
            // labelProgress
            // 
            this.labelProgress.AutoSize = true;
            this.labelProgress.Location = new System.Drawing.Point(195, 459);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(51, 13);
            this.labelProgress.TabIndex = 2;
            this.labelProgress.Text = "Progress:";
            // 
            // textBoxImgDir
            // 
            this.textBoxImgDir.Location = new System.Drawing.Point(72, 7);
            this.textBoxImgDir.Name = "textBoxImgDir";
            this.textBoxImgDir.Size = new System.Drawing.Size(435, 20);
            this.textBoxImgDir.TabIndex = 5;
            this.textBoxImgDir.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxImgDir_KeyPress);
            // 
            // buttonNext
            // 
            this.buttonNext.Location = new System.Drawing.Point(464, 454);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(75, 23);
            this.buttonNext.TabIndex = 7;
            this.buttonNext.Text = "Next >>";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.ButtonNext_Click);
            // 
            // buttonRemove
            // 
            this.buttonRemove.Location = new System.Drawing.Point(552, 323);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(66, 23);
            this.buttonRemove.TabIndex = 8;
            this.buttonRemove.Text = "Remove";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.ButtonRemove_Click);
            // 
            // buttonDelAll
            // 
            this.buttonDelAll.Location = new System.Drawing.Point(630, 323);
            this.buttonDelAll.Name = "buttonDelAll";
            this.buttonDelAll.Size = new System.Drawing.Size(71, 23);
            this.buttonDelAll.TabIndex = 9;
            this.buttonDelAll.Text = "Delete All";
            this.buttonDelAll.UseVisualStyleBackColor = true;
            this.buttonDelAll.Click += new System.EventHandler(this.ButtonDelAll_Click);
            // 
            // textBoxProcessNo
            // 
            this.textBoxProcessNo.BackColor = System.Drawing.SystemColors.MenuBar;
            this.textBoxProcessNo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxProcessNo.Location = new System.Drawing.Point(250, 460);
            this.textBoxProcessNo.Name = "textBoxProcessNo";
            this.textBoxProcessNo.Size = new System.Drawing.Size(91, 13);
            this.textBoxProcessNo.TabIndex = 11;
            this.textBoxProcessNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextboxProcessNo_KeyPress);
            // 
            // buttonOpenDialog
            // 
            this.buttonOpenDialog.Location = new System.Drawing.Point(507, 5);
            this.buttonOpenDialog.Name = "buttonOpenDialog";
            this.buttonOpenDialog.Size = new System.Drawing.Size(32, 23);
            this.buttonOpenDialog.TabIndex = 15;
            this.buttonOpenDialog.Text = "...";
            this.buttonOpenDialog.UseVisualStyleBackColor = true;
            this.buttonOpenDialog.Click += new System.EventHandler(this.ButtonOpenDialog_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(12, 34);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(529, 417);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.PictureBox1_Paint);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PictureBox1_MouseDown);
            this.pictureBox1.MouseEnter += new System.EventHandler(this.PictureBox1_MouseEnter);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.PictureBox1_MouseLeave);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PictureBox1_MouseUp);
            // 
            // labelMousePosition
            // 
            this.labelMousePosition.AutoSize = true;
            this.labelMousePosition.Location = new System.Drawing.Point(12, 459);
            this.labelMousePosition.Name = "labelMousePosition";
            this.labelMousePosition.Size = new System.Drawing.Size(32, 13);
            this.labelMousePosition.TabIndex = 18;
            this.labelMousePosition.Text = "x: , y:";
            // 
            // listBoxBoundary
            // 
            this.listBoxBoundary.FormattingEnabled = true;
            this.listBoxBoundary.Location = new System.Drawing.Point(553, 79);
            this.listBoxBoundary.Name = "listBoxBoundary";
            this.listBoxBoundary.Size = new System.Drawing.Size(149, 238);
            this.listBoxBoundary.TabIndex = 20;
            this.listBoxBoundary.SelectedIndexChanged += new System.EventHandler(this.ListBoxBoundary_SelectedIndexChanged);
            // 
            // labelBoundary
            // 
            this.labelBoundary.AutoSize = true;
            this.labelBoundary.Location = new System.Drawing.Point(554, 62);
            this.labelBoundary.Name = "labelBoundary";
            this.labelBoundary.Size = new System.Drawing.Size(71, 13);
            this.labelBoundary.TabIndex = 21;
            this.labelBoundary.Text = "Boundary List";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(550, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Label:";
            // 
            // buttonVideo
            // 
            this.buttonVideo.Location = new System.Drawing.Point(543, 454);
            this.buttonVideo.Name = "buttonVideo";
            this.buttonVideo.Size = new System.Drawing.Size(75, 23);
            this.buttonVideo.TabIndex = 24;
            this.buttonVideo.Text = "Video Mode";
            this.buttonVideo.UseVisualStyleBackColor = true;
            this.buttonVideo.Click += new System.EventHandler(this.ButtonVideo_Click);
            // 
            // buttonCapture
            // 
            this.buttonCapture.Enabled = false;
            this.buttonCapture.Location = new System.Drawing.Point(622, 454);
            this.buttonCapture.Name = "buttonCapture";
            this.buttonCapture.Size = new System.Drawing.Size(75, 23);
            this.buttonCapture.TabIndex = 25;
            this.buttonCapture.Text = "Capture";
            this.buttonCapture.UseVisualStyleBackColor = true;
            this.buttonCapture.Click += new System.EventHandler(this.ButtonCapture_Click);
            // 
            // buttonDeleteImage
            // 
            this.buttonDeleteImage.Location = new System.Drawing.Point(567, 352);
            this.buttonDeleteImage.Name = "buttonDeleteImage";
            this.buttonDeleteImage.Size = new System.Drawing.Size(114, 23);
            this.buttonDeleteImage.TabIndex = 26;
            this.buttonDeleteImage.Text = "Delete Image";
            this.buttonDeleteImage.UseVisualStyleBackColor = true;
            this.buttonDeleteImage.Click += new System.EventHandler(this.ButtonDeleteImage_Click);
            // 
            // checkBoxStereoCamera
            // 
            this.checkBoxStereoCamera.AutoSize = true;
            this.checkBoxStereoCamera.Enabled = false;
            this.checkBoxStereoCamera.Location = new System.Drawing.Point(553, 43);
            this.checkBoxStereoCamera.Name = "checkBoxStereoCamera";
            this.checkBoxStereoCamera.Size = new System.Drawing.Size(96, 17);
            this.checkBoxStereoCamera.TabIndex = 27;
            this.checkBoxStereoCamera.Text = "Stereo Camera";
            this.checkBoxStereoCamera.UseVisualStyleBackColor = true;
            this.checkBoxStereoCamera.CheckedChanged += new System.EventHandler(this.CheckBoxStereoCamera_CheckedChanged);
            // 
            // checkBoxLeft
            // 
            this.checkBoxLeft.AutoSize = true;
            this.checkBoxLeft.Enabled = false;
            this.checkBoxLeft.Location = new System.Drawing.Point(649, 34);
            this.checkBoxLeft.Name = "checkBoxLeft";
            this.checkBoxLeft.Size = new System.Drawing.Size(44, 17);
            this.checkBoxLeft.TabIndex = 28;
            this.checkBoxLeft.Text = "Left";
            this.checkBoxLeft.UseVisualStyleBackColor = true;
            // 
            // checkBoxRight
            // 
            this.checkBoxRight.AutoSize = true;
            this.checkBoxRight.Enabled = false;
            this.checkBoxRight.Location = new System.Drawing.Point(649, 56);
            this.checkBoxRight.Name = "checkBoxRight";
            this.checkBoxRight.Size = new System.Drawing.Size(51, 17);
            this.checkBoxRight.TabIndex = 29;
            this.checkBoxRight.Text = "Right";
            this.checkBoxRight.UseVisualStyleBackColor = true;
            // 
            // buttonGenerateList
            // 
            this.buttonGenerateList.Location = new System.Drawing.Point(567, 381);
            this.buttonGenerateList.Name = "buttonGenerateList";
            this.buttonGenerateList.Size = new System.Drawing.Size(114, 23);
            this.buttonGenerateList.TabIndex = 30;
            this.buttonGenerateList.Text = "Generate List";
            this.buttonGenerateList.UseVisualStyleBackColor = true;
            this.buttonGenerateList.Click += new System.EventHandler(this.ButtonGenerateList_Click);
            // 
            // buttonStartTraining
            // 
            this.buttonStartTraining.Location = new System.Drawing.Point(567, 410);
            this.buttonStartTraining.Name = "buttonStartTraining";
            this.buttonStartTraining.Size = new System.Drawing.Size(114, 23);
            this.buttonStartTraining.TabIndex = 31;
            this.buttonStartTraining.Text = "Start Training";
            this.buttonStartTraining.UseVisualStyleBackColor = true;
            this.buttonStartTraining.Click += new System.EventHandler(this.ButtonStartTraining_Click);
            // 
            // buttonEdit
            // 
            this.buttonEdit.Location = new System.Drawing.Point(662, 6);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(40, 23);
            this.buttonEdit.TabIndex = 32;
            this.buttonEdit.Text = "Edit";
            this.buttonEdit.UseVisualStyleBackColor = true;
            this.buttonEdit.Click += new System.EventHandler(this.ButtonEdit_Click);
            // 
            // comboBoxListLabel
            // 
            this.comboBoxListLabel.FormattingEnabled = true;
            this.comboBoxListLabel.Location = new System.Drawing.Point(581, 7);
            this.comboBoxListLabel.Name = "comboBoxListLabel";
            this.comboBoxListLabel.Size = new System.Drawing.Size(83, 21);
            this.comboBoxListLabel.TabIndex = 33;
            // 
            // checkBoxTestImage
            // 
            this.checkBoxTestImage.AutoSize = true;
            this.checkBoxTestImage.Location = new System.Drawing.Point(552, 434);
            this.checkBoxTestImage.Name = "checkBoxTestImage";
            this.checkBoxTestImage.Size = new System.Drawing.Size(74, 17);
            this.checkBoxTestImage.TabIndex = 34;
            this.checkBoxTestImage.Text = "test image";
            this.checkBoxTestImage.UseVisualStyleBackColor = true;
            this.checkBoxTestImage.CheckedChanged += new System.EventHandler(this.checkBoxTestImage_CheckedChanged);
            // 
            // MainForm
            // 
            this.ClientSize = new System.Drawing.Size(713, 483);
            this.Controls.Add(this.checkBoxTestImage);
            this.Controls.Add(this.comboBoxListLabel);
            this.Controls.Add(this.buttonEdit);
            this.Controls.Add(this.buttonStartTraining);
            this.Controls.Add(this.buttonGenerateList);
            this.Controls.Add(this.checkBoxRight);
            this.Controls.Add(this.checkBoxLeft);
            this.Controls.Add(this.checkBoxStereoCamera);
            this.Controls.Add(this.buttonDeleteImage);
            this.Controls.Add(this.buttonCapture);
            this.Controls.Add(this.buttonVideo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelBoundary);
            this.Controls.Add(this.listBoxBoundary);
            this.Controls.Add(this.labelMousePosition);
            this.Controls.Add(this.buttonOpenDialog);
            this.Controls.Add(this.textBoxProcessNo);
            this.Controls.Add(this.buttonDelAll);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.buttonNext);
            this.Controls.Add(this.textBoxImgDir);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.LabelImageDir);
            this.Controls.Add(this.buttonPrev);
            this.Name = "MainForm";
            this.Text = "Label-tool";
            this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
            this.SizeChanged += new System.EventHandler(this.MainForm_ResizeEnd);
            this.Resize += new System.EventHandler(this.FromResize);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        

        private System.Windows.Forms.Button buttonPrev;
        private System.Windows.Forms.Label LabelImageDir;
        private System.Windows.Forms.Label labelProgress;
        private System.Windows.Forms.TextBox textBoxImgDir;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Button buttonDelAll;
        private System.Windows.Forms.TextBox textBoxProcessNo;
        private System.Windows.Forms.Button buttonOpenDialog;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelMousePosition;
        private System.Windows.Forms.ListBox listBoxBoundary;
        private System.Windows.Forms.Label labelBoundary;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonVideo;
        private System.Windows.Forms.Button buttonCapture;
        private System.Windows.Forms.Button buttonDeleteImage;
        private System.Windows.Forms.CheckBox checkBoxStereoCamera;
        private System.Windows.Forms.CheckBox checkBoxLeft;
        private System.Windows.Forms.CheckBox checkBoxRight;
        private System.Windows.Forms.Button buttonGenerateList;
        private System.Windows.Forms.Button buttonStartTraining;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.ComboBox comboBoxListLabel;
        private System.Windows.Forms.CheckBox checkBoxTestImage;
    }
}

