﻿namespace Label_Tool_master
{
    partial class LabelConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAddLabel = new System.Windows.Forms.Button();
            this.labelSetLabel = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.textBoxNewLabel = new System.Windows.Forms.TextBox();
            this.labelNewLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonAddLabel
            // 
            this.buttonAddLabel.Location = new System.Drawing.Point(15, 282);
            this.buttonAddLabel.Name = "buttonAddLabel";
            this.buttonAddLabel.Size = new System.Drawing.Size(84, 23);
            this.buttonAddLabel.TabIndex = 0;
            this.buttonAddLabel.Text = "Add Label";
            this.buttonAddLabel.UseVisualStyleBackColor = true;
            this.buttonAddLabel.Click += new System.EventHandler(this.buttonAddLabel_Click);
            // 
            // labelSetLabel
            // 
            this.labelSetLabel.AutoSize = true;
            this.labelSetLabel.Location = new System.Drawing.Point(12, 9);
            this.labelSetLabel.Name = "labelSetLabel";
            this.labelSetLabel.Size = new System.Drawing.Size(69, 13);
            this.labelSetLabel.TabIndex = 3;
            this.labelSetLabel.Text = "Set of Labels";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 25);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(198, 225);
            this.listBox1.TabIndex = 4;
            // 
            // buttonRemove
            // 
            this.buttonRemove.Location = new System.Drawing.Point(120, 282);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(90, 23);
            this.buttonRemove.TabIndex = 5;
            this.buttonRemove.Text = "Remove Label";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // textBoxNewLabel
            // 
            this.textBoxNewLabel.Location = new System.Drawing.Point(67, 256);
            this.textBoxNewLabel.Name = "textBoxNewLabel";
            this.textBoxNewLabel.Size = new System.Drawing.Size(143, 20);
            this.textBoxNewLabel.TabIndex = 6;
            // 
            // labelNewLabel
            // 
            this.labelNewLabel.AutoSize = true;
            this.labelNewLabel.Location = new System.Drawing.Point(9, 259);
            this.labelNewLabel.Name = "labelNewLabel";
            this.labelNewLabel.Size = new System.Drawing.Size(58, 13);
            this.labelNewLabel.TabIndex = 7;
            this.labelNewLabel.Text = "New Label";
            // 
            // LabelConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(225, 314);
            this.Controls.Add(this.labelNewLabel);
            this.Controls.Add(this.textBoxNewLabel);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.labelSetLabel);
            this.Controls.Add(this.buttonAddLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "LabelConfig";
            this.Text = "Label Configuration";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAddLabel;
        private System.Windows.Forms.Label labelSetLabel;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.TextBox textBoxNewLabel;
        private System.Windows.Forms.Label labelNewLabel;
    }
}