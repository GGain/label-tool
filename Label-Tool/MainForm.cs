﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Drawing2D;
using System.Diagnostics;

using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.Util;

namespace Label_Tool_master
{
    public partial class MainForm : Form
    {
        private string imgDir;
        private List<string> allImagesFound;
        private List<string> testList = new List<string>();
        private List<string> trainList = new List<string>();
        private string cordTextFileName;
        private string imageFileName;
        private int imgCnt = 0;
        private bool drawRectangelFlag = false;
        private bool videoFlag = false;
        private bool _captureInProgress = false;

        private static string OUTPUT_CAPTURE_DIRECTORY = @"Capture";
        private static string CAPTURE_IMAGE_NAME = @"Capture";
        private static string TEST_MARK = @"--test--";
        private static string TRAIN_LIST_FILE = @"train.txt";
        private static string TEST_LIST_FILE = @"test.txt";
        private static string OBJ_NAMES_FILE = @"obj.names";
        private static string OBJ_DATA_FILE = @"obj.data";
        private static string All_IMAGE_FILE = @"ListofImageDirectory.txt";

        private Point mouseUp;
        private Point mouseDown;
        private Point mouseCurrentPosition;
        private bool MOUSE_FLAG = false;
        private Point offsetPoint = new Point(0, 0);
        private int listBoxBoundaryPreSelected = -1;
        private float scale = 1;
        private Rectangle rect = new Rectangle(125, 125, 50, 50);
        private List<int> objNo = new List<int>();

        private VideoCapture capture = null;
        private Mat _frame;

        public MainForm()
        {
            InitializeComponent();
            
        }
        private void FromResize(object sender, EventArgs e)
        {
            buttonEdit.Location = new Point(this.Width - buttonEdit.Width - 20, 10);
            comboBoxListLabel.Location=new Point(buttonEdit.Location.X - comboBoxListLabel.Width, 10);
            label1.Location = new Point(comboBoxListLabel.Location.X - label1.Width - 5, 10);

            checkBoxLeft.Location = new Point(this.Width - checkBoxRight.Width - 20, 
                                                label1.Location.Y + label1.Height + 10);
            checkBoxRight.Location = new Point(this.Width - checkBoxRight.Width - 20, 
                                                checkBoxLeft.Location.Y + checkBoxLeft.Height + 2);
            checkBoxStereoCamera.Location = new Point(label1.Location.X,
                                                checkBoxRight.Location.Y - checkBoxStereoCamera.Height / 2 - 2);

            labelBoundary.Location = new Point(label1.Location.X, checkBoxRight.Location.Y + 10);
            listBoxBoundary.Location = new Point(labelBoundary.Location.X,
                                                    labelBoundary.Location.Y+labelBoundary.Height + 5);
            buttonRemove.Location = new Point(listBoxBoundary.Location.X, 
                                                    listBoxBoundary.Location.Y + listBoxBoundary.Height + 5);
            buttonDelAll.Location = new Point(listBoxBoundary.Location.X + listBoxBoundary.Width - buttonDelAll.Width,
                                                    buttonRemove.Location.Y);
            buttonDeleteImage.Location = new Point(listBoxBoundary.Location.X + listBoxBoundary.Width / 2 - buttonDeleteImage.Width / 2,
                                                    buttonDelAll.Location.Y + buttonDelAll.Height + 5);
            buttonGenerateList.Location = new Point(buttonDeleteImage.Location.X,
                                                    buttonDeleteImage.Location.Y + buttonDeleteImage.Height + 5);
            buttonStartTraining.Location = new Point(buttonGenerateList.Location.X,
                                                    buttonGenerateList.Location.Y + buttonStartTraining.Height + 5);

            buttonNext.Location = new Point(this.Width / 2 + 5, this.Height - buttonNext.Height - 50);
            buttonPrev.Location = new Point(this.Width / 2 - 5 - buttonPrev.Width, this.Height - buttonNext.Height - 50);
            buttonCapture.Location = new Point(this.Width - buttonCapture.Width - 20, this.Height - buttonNext.Height - 50);
            buttonVideo.Location = new Point(buttonCapture.Location.X - buttonVideo.Width - 10, this.Height - buttonNext.Height - 50);
            checkBoxTestImage.Location = new Point(buttonNext.Location.X + buttonNext.Width + 10, buttonNext.Location.Y + buttonNext.Height/2- checkBoxTestImage.Height/2);

            textBoxProcessNo.Location = new Point(this.Width / 4, this.Height - labelProgress.Height - 50);
            labelProgress.Location = new Point(this.Width / 4 - labelProgress.Width, this.Height - labelProgress.Height - 50);
            labelMousePosition.Location = new Point(labelMousePosition.Location.X, this.Height - labelProgress.Height - 50);

            pictureBox1.Size = new Size(listBoxBoundary.Location.X - 20, buttonNext.Location.Y - 40);

        }
        
        private void _updateProcess(int i)
        {
            textBoxProcessNo.Text = (i + 1).ToString() + "/" + allImagesFound.Count.ToString();
        }

        private void _loadImg(int i)
        {
            try
            {
                if (allImagesFound.Count != 0)
                {
                    string imageName = Path.GetFileName(allImagesFound[i]);
                    //load from file stream to avoid lock image file
                    System.IO.FileStream fs = new System.IO.FileStream(allImagesFound[i], System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    //if(pictureBox1.Image != null) pictureBox1.Image.Dispose();
                    pictureBox1.Image = System.Drawing.Image.FromStream(fs);
                    fs.Close();

                    if (allImagesFound[i].Contains(TEST_MARK))
                    {
                        checkBoxTestImage.Checked = true;
                    }
                    else
                    {
                        checkBoxTestImage.Checked = false;
                    }

                    // Change the app title
                    this.Text = string.Format("{0}:Label-tool", imageName);

                    _loadCordinate(i);
                    _caculateScaleOffset();
                }                      

            }
            catch (Exception evt)
            {
                string msg = string.Format(@"Error when laoding image!!! {1}{0}", evt.ToString(), Environment.NewLine);
                MessageBox.Show(msg);
            }
        }

        private void _caculateScaleOffset()
        {
            float diffRatio = (float)pictureBox1.Image.Width / pictureBox1.Width - (float)pictureBox1.Image.Height / pictureBox1.Height;
            if (diffRatio > 0)
            {
                scale = (float)pictureBox1.Width / pictureBox1.Image.Width;
                offsetPoint.X = 0;
                offsetPoint.Y = (int)(pictureBox1.Height / 2 - pictureBox1.Image.Height * scale / 2);
            }
            else
            {
                scale = (float)pictureBox1.Height / pictureBox1.Image.Height;
                offsetPoint.X = (int)(pictureBox1.Width / 2 - pictureBox1.Image.Width * scale / 2);
                offsetPoint.Y = 0;
            }
        }

        private void _loadCordinate(int i )
        {
            imageFileName = Path.GetFileNameWithoutExtension(allImagesFound[i]);
            cordTextFileName = imgDir + "\\" + imageFileName + ".txt";
            
            Graphics recGrahp = Graphics.FromImage(pictureBox1.Image);
            Pen redPen = new Pen(Color.FromArgb(255, 255, 0, 0), 4);
            SolidBrush redBrush = new SolidBrush(Color.Red);
            Font drawFont = new Font("Arial", 16);

            //clear the listBox
            listBoxBoundary.Items.Clear();

            if (File.Exists(cordTextFileName))
            {
                if (objNo != null) objNo.Clear();
                string[] lines = File.ReadAllLines(cordTextFileName);
                if (lines != null)
                {
                    foreach (string line in lines)
                    {
                        if (line.Length != 5)
                        {
                            try
                            {
                                string[] cordinate = line.Split(' ');
                                objNo.Add(Int32.Parse(cordinate[0]));
                                comboBoxListLabel.SelectedIndex = Int32.Parse(cordinate[0]);
                                int w = (int)(Convert.ToDouble(cordinate[3]) * pictureBox1.Image.Width);
                                int h = (int)(Convert.ToDouble(cordinate[4]) * pictureBox1.Image.Height);
                                int x = (int)(Convert.ToDouble(cordinate[1]) * pictureBox1.Image.Width - w / 2);
                                int y = (int)(Convert.ToDouble(cordinate[2]) * pictureBox1.Image.Height - h / 2);
                                Rectangle rec = new Rectangle(x,y,w,h);
                                if (rec.X < 0)
                                {
                                    rec.X = 0;
                                }
                                if (rec.X > pictureBox1.Image.Width)
                                {
                                    rec.X = pictureBox1.Image.Width;
                                }
                                if (rec.X + rec.Width > pictureBox1.Image.Width)
                                {
                                    rec.Width = pictureBox1.Image.Width - rec.X;
                                }
                                if (rec.Y < 0)
                                {
                                    rec.Y = 0;
                                }
                                if (rec.Y > pictureBox1.Image.Height)
                                {
                                    rec.Y = pictureBox1.Image.Height;
                                }
                                if (rec.Y + rec.Height > pictureBox1.Image.Height)
                                {
                                    rec.Height = pictureBox1.Image.Height - rec.Height;
                                }
                                recGrahp.DrawRectangle(redPen, x, y, w, h);
                                recGrahp.DrawString(string.Format("{0}({1},{2})", comboBoxListLabel.SelectedItem.ToString(), x, y), drawFont, redBrush, rec.Location);
                                listBoxBoundary.Items.Add(rec);
                            }
                            catch(Exception e)
                            {
                                MessageBox.Show(string.Format("Wrong text file format!{0}{1}",Environment.NewLine,e.ToString()));
                            }
                        }
                        else
                        {
                            MessageBox.Show("Syntax in each text file should look like:0 1 1 2 2!!!");
                        }
                    }
                }

            }
            redPen.Dispose();
            redBrush.Dispose();
            drawFont.Dispose();
        }
        
        private void _loadImgAndImageNames(bool showImageFlag)
        {
            //Support image type
            string[] filters = filters = new String[] { "jpg", "jpeg", "png", "gif", "tiff", "bmp" };
            //List of found images
            allImagesFound = new List<string>();
            if (!string.IsNullOrWhiteSpace(imgDir) && Directory.Exists(imgDir))
            {
                foreach (var filter in filters)
                {
                    allImagesFound.AddRange(Directory.GetFiles(imgDir, String.Format("*.{0}", filter)));
                }
                //Get all the image names
                //allImagesFound = imagesFound.ToArray();

                foreach (string file in allImagesFound)
                {
                    if (file.Contains(TEST_MARK))
                    {
                        testList.Add(file);
                    }
                    else
                    {
                        trainList.Add(file);
                    }
                }
                
                LoadLabelList();

                imgCnt = 0;
                if (showImageFlag)
                {
                    _updateProcess(imgCnt);
                    _loadImg(imgCnt);
                }
                
            }
            else
            {
                MessageBox.Show("Directory is invalid!!!!");
            }
        }
        
        private void TextBoxImgDir_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxImgDir.Text)) return;
            if (e.KeyChar == (char)Keys.Enter)
            {
                imgDir = textBoxImgDir.Text;
                //Load all image in the folder when in image mode.
                //When in the video mode, this is output folder.
                if (!videoFlag)
                {
                    _loadImgAndImageNames(true);
                }
                
            }
        }

        private void TextboxProcessNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((allImagesFound == null) || (e.KeyChar != (char)Keys.Enter) ||
                string.IsNullOrWhiteSpace(textBoxProcessNo.Text))
            {
                return;
            }

            try
            {
                if (textBoxProcessNo.Text.Contains('/') && textBoxProcessNo.Text.IndexOf('/') != 0)
                {
                    imgCnt = Convert.ToInt32(textBoxProcessNo.Text.Split('/')[0]) - 1;
                }
                else
                {
                    imgCnt = Convert.ToInt32(textBoxProcessNo.Text) - 1;
                }
                if (0 <= imgCnt && imgCnt < allImagesFound.Count)
                {
                    _loadImg(imgCnt);
                    _updateProcess(imgCnt);
                }
                else
                {
                    string msg = string.Format(@"The index of the image have to positive interger and less than {0}!", allImagesFound.Count.ToString());
                    MessageBox.Show(msg);
                }
            }
            catch
            {
                MessageBox.Show("Please insert the interger number only!!!");
            }

        }

        private void ButtonOpenDialog_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog openFileDialog1 = new FolderBrowserDialog();
            string targetDirectory;

            openFileDialog1.SelectedPath = Directory.GetCurrentDirectory();
            
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (openFileDialog1.SelectedPath != null)
                {
                    targetDirectory = (openFileDialog1.SelectedPath);
                    textBoxImgDir.Text = targetDirectory;
                    imgDir = textBoxImgDir.Text;
                    if (!videoFlag)
                    {
                        //load all image when in image mode. 
                        _loadImgAndImageNames(true);
                    }
                    
                    //string[] filters = filters = new String[] { "jpg", "jpeg", "png", "gif", "tiff", "bmp" };
                    //List<string> imagesFound = new List<string>();
                    //foreach (var filter in filters)
                    //{
                    //    imagesFound.AddRange(Directory.GetFiles(targetDirectory, String.Format("*.{0}", filter)));
                    //}
                    ////Get all the image names
                    //allImagesFound = imagesFound.ToArray();
                    //imgCnt = 0;
                }
            }
        }

        private void ButtonNext_Click(object sender, EventArgs e)
        {
            if (allImagesFound != null)
            {
                imgCnt++;
                if (imgCnt >= allImagesFound.Count)
                {
                    imgCnt = 0;
                }

                _updateProcess(imgCnt);

                _loadImg(imgCnt);
            }
        }

        private void ButtonPrev_Click(object sender, EventArgs e)
        {
            if (allImagesFound != null)
            {
                imgCnt--;
                if (imgCnt < 0)
                {
                    imgCnt = allImagesFound.Count - 1;
                }

                _updateProcess(imgCnt);

                _loadImg(imgCnt);
            }

        }

        private void ButtonDelAll_Click(object sender, EventArgs e)
        {
            if (listBoxBoundary.Items.Count != 0)
            {
                listBoxBoundary.Items.Clear();
                if (File.Exists(cordTextFileName))
                {
                    File.Delete(cordTextFileName);
                    _loadImg(imgCnt);
                }
            }
        }

        private void ButtonRemove_Click(object sender, EventArgs e)
        {
            if (listBoxBoundary.SelectedIndex != -1 && listBoxBoundary.Items.Count != 0)
            {
                listBoxBoundaryPreSelected = -1;
                //remove the obj in the list
                objNo.RemoveAt(listBoxBoundary.SelectedIndex);
                listBoxBoundary.Items.RemoveAt(listBoxBoundary.SelectedIndex);
                
                string[] text = new string[listBoxBoundary.Items.Count];
                int i = 0;
                foreach (Rectangle item in listBoxBoundary.Items)
                {

                    float wf = (float)(item.Width) / pictureBox1.Image.Width;
                    float hf = (float)(item.Height) / pictureBox1.Image.Height;

                    float cxf = (float)(item.X + item.Width / 2) / pictureBox1.Image.Width;
                    float cyf = (float)(item.Y + item.Height / 2) / pictureBox1.Image.Height;
                    string cordinateNormText = objNo[i] + " " + cxf + " " + cyf + " " + wf + " " + hf;
                    text[i] = cordinateNormText;
                    i++;
                }
                File.WriteAllLines(cordTextFileName, text);
#if DEBUG
                Console.WriteLine("Box write to file");
                Console.WriteLine(text);
#endif
                _loadImg(imgCnt);
            }
        }
        
        private void PictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (pictureBox1.Image == null) return;
            labelMousePosition.Text = "x:" + e.X + ", y:" + e.Y;
            mouseCurrentPosition = new Point(e.X, e.Y);
            if (drawRectangelFlag == true)
            {
                Point topLeft = new Point(Math.Min(mouseDown.X, e.X), Math.Min(mouseDown.Y, e.Y));
                Point botRight = new Point(Math.Max(mouseDown.X, e.X), Math.Max(mouseDown.Y, e.Y));
                
                rect = new Rectangle(topLeft, new Size(botRight.X - topLeft.X, botRight.Y - topLeft.Y));
                
            }
            if (MOUSE_FLAG)
            {
                // this.Cursor = crossCursor(new Pen(Color.Black, 1), new SolidBrush(Color.Red), "giang", e.X, e.Y);
                pictureBox1.Invalidate();
            }
        }

        //The follow commnet code use to change the cursor type
        /*
        private Cursor crossCursor(Pen pen, Brush brush, string name, int x, int y)
        {
            var pic = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            Graphics gr = Graphics.FromImage(pic);

            var pathX = new GraphicsPath();
            var pathY = new GraphicsPath();

            pathX.AddLine(0, pictureBox1.Height/2, pictureBox1.Width, pictureBox1.Height/2);
            pathY.AddLine(pictureBox1.Width / 2, 0, pictureBox1.Width / 2, pictureBox1.Height);

            gr.DrawPath(pen, pathX);
            gr.DrawPath(pen, pathY);
            //gr.DrawString(name, Font, brush, x / 2 + 5, y - 35);

            IntPtr ptr = pic.GetHicon();
            var c = new Cursor(ptr);
            return c;
        }
        */
        private void PictureBox1_MouseEnter(object sender, EventArgs e)
        {
            if (pictureBox1.Image == null || (buttonVideo.Text == "Image Mode" && !_captureInProgress)) return;
            if (!MOUSE_FLAG)
            {
                MOUSE_FLAG = true;
            }
        }

        private void PictureBox1_MouseLeave(object sender, EventArgs e)
        {
            if (MOUSE_FLAG)
            {
                MOUSE_FLAG = false;
                //this.Cursor = Cursors.Default;
                pictureBox1.Refresh();
            }
        }

        private void PictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (pictureBox1.Image == null || (videoFlag && !_captureInProgress)) return;
            if (e.Button == MouseButtons.Left)
            {
                drawRectangelFlag = true;
                mouseDown = new Point(e.X,e.Y);
            }
        }

        private void PictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (pictureBox1.Image == null || (videoFlag && !_captureInProgress)) return;

            if (e.Button == MouseButtons.Left)
            {
                drawRectangelFlag = false;
                if (comboBoxListLabel.Items.Count == 0)
                {
                    MessageBox.Show("Please add label!");
                }
                else
                {
                    mouseUp = new Point(e.X, e.Y);

                    Point topLeft = new Point(Math.Min(mouseDown.X, mouseUp.X), Math.Min(mouseDown.Y, mouseUp.Y));
                    Point botRight = new Point(Math.Max(mouseDown.X, mouseUp.X), Math.Max(mouseDown.Y, mouseUp.Y));

                    int x = (int)((topLeft.X - offsetPoint.X) / scale);
                    int w = (int)((botRight.X - topLeft.X) / scale);
                    int y = (int)((topLeft.Y - offsetPoint.Y) / scale);
                    int h = (int)((botRight.Y - topLeft.Y) / scale);

                    float cxf = (float)(x + (float)w / 2) / pictureBox1.Image.Width;
                    float cyf = (float)(y + (float)h / 2) / pictureBox1.Image.Height;

                    float wf = (float)(w) / pictureBox1.Image.Width;
                    float hf = (float)(h) / pictureBox1.Image.Height;

                    Rectangle rec = new Rectangle(x, y, w, h);
                    if (rec.X < 0)
                    {
                        rec.X = 0;
                    }
                    if (rec.X > pictureBox1.Image.Width)
                    {
                        rec.X = pictureBox1.Image.Width;
                    }
                    if (rec.X + rec.Width > pictureBox1.Image.Width)
                    {
                        rec.Width = pictureBox1.Image.Width - rec.X;
                    }
                    if (rec.Y < 0)
                    {
                        rec.Y = 0;
                    }
                    if (rec.Y > pictureBox1.Image.Height)
                    {
                        rec.Y = pictureBox1.Image.Height;
                    }
                    if (rec.Y + rec.Height > pictureBox1.Image.Height)
                    {
                        rec.Height = pictureBox1.Image.Height - rec.Height;
                    }
                    Graphics recGrahp = Graphics.FromImage(pictureBox1.Image);
                    Pen redPen = new Pen(Color.FromArgb(255, 255, 0, 0), 4);
                    SolidBrush redBrush = new SolidBrush(Color.Red);
                    Font drawFont = new Font("Arial", 16);

                    recGrahp.DrawRectangle(redPen, rec);
                    PointF textPosition = rec.Location;
                    if (textPosition.Y < 0)
                    {
                        textPosition.Y = y;
                    }

                    recGrahp.DrawString(string.Format("{0}({1},{2})", comboBoxListLabel.SelectedItem.ToString(), x, y), drawFont, redBrush, textPosition);
                    objNo.Add(comboBoxListLabel.SelectedIndex);
                    listBoxBoundary.Items.Add(rec);
                    string cordinateNormText = string.Format("{0} {1} {2} {3} {4}", comboBoxListLabel.SelectedIndex, cxf, cyf, wf, hf);
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(cordTextFileName, true))
                    {
                        file.WriteLine(cordinateNormText);
                    }

                    pictureBox1.Refresh();

                    redPen.Dispose();
                    redBrush.Dispose();
                    drawFont.Dispose();
                }
            }
        }

        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (drawRectangelFlag == true)
            {
                pictureBox1.Invalidate();
                Pen redPen = new Pen(Color.FromArgb(255, 255, 0, 0), 2);
                e.Graphics.DrawRectangle(redPen, rect);
            }
            if (MOUSE_FLAG)
            {
                e.Graphics.DrawLine(new Pen(Color.Black, 1), mouseCurrentPosition.X, 0, mouseCurrentPosition.X, pictureBox1.Height);
                e.Graphics.DrawLine(new Pen(Color.Black, 1), 0, mouseCurrentPosition.Y, pictureBox1.Width, mouseCurrentPosition.Y);
            }
        }
        
        private void MainForm_ResizeEnd(object sender, EventArgs e)
        {
            if (pictureBox1.Image != null)
            {
                float diffRatio = (float)pictureBox1.Image.Width / pictureBox1.Width - (float)pictureBox1.Image.Height / pictureBox1.Height;
                if (diffRatio > 0)
                {
                    scale = (float)pictureBox1.Width / pictureBox1.Image.Width;
                    offsetPoint.X = 0;
                    offsetPoint.Y = (int)(pictureBox1.Height / 2 - pictureBox1.Image.Height * scale / 2);
                }
                else
                {
                    scale = (float)pictureBox1.Height / pictureBox1.Image.Height;
                    offsetPoint.X = (int)(pictureBox1.Width / 2 - pictureBox1.Image.Width * scale / 2);
                    offsetPoint.Y = 0;
                }
            }
        }
        
        private void ListBoxBoundary_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            Graphics recGraph = Graphics.FromImage(pictureBox1.Image);
            Pen redPen = new Pen(Color.Red, 4);
            Pen bluePen = new Pen(Color.Blue, 4);

            if (listBoxBoundaryPreSelected != -1)
            {
                recGraph.DrawRectangle(redPen, (Rectangle) listBoxBoundary.Items[listBoxBoundaryPreSelected]);
            }
            
            listBoxBoundaryPreSelected = listBoxBoundary.SelectedIndex;
            if (listBoxBoundary.SelectedIndex != -1)
            {
                recGraph.DrawRectangle(bluePen, (Rectangle) listBoxBoundary.SelectedItem);
            }
            
            pictureBox1.Invalidate();
        }
        
        private void ProcessFrame(object sender, EventArgs arg)
        {
            if (capture != null && capture.Ptr != IntPtr.Zero && videoFlag)
            {
                capture.Retrieve(_frame, 0);
                Rectangle tmpLeftROI = new Rectangle(0, 0, _frame.Cols / 2, _frame.Rows);
                Rectangle tmpRightROI = new Rectangle(_frame.Cols / 2, 0, _frame.Cols/2, _frame.Rows);
                
                //if (pictureBox1.Image != null) pictureBox1.Image.Dispose();
                
                if (checkBoxLeft.Checked && checkBoxRight.Checked || !checkBoxStereoCamera.Checked)
                {
                    pictureBox1.Image = _frame.Bitmap;
                }
                else if (checkBoxRight.Checked)
                {
                    Mat right_image = new Mat(_frame, tmpRightROI);
                    pictureBox1.Image = right_image.Bitmap;
                }
                else if (checkBoxLeft.Checked)
                {
                    Mat left_image = new Mat(_frame, tmpLeftROI);
                    pictureBox1.Image = left_image.Bitmap;
                }
                else
                {
                    Mat left_image = new Mat(_frame, tmpLeftROI);
                    pictureBox1.Image = left_image.Bitmap;
                }
                
                
            }
            
        }

        private void ButtonCapture_Click(object sender, EventArgs e)
        {
            if (capture != null && videoFlag)
            {
                //clear the listBox
                listBoxBoundary.Items.Clear();

                _captureInProgress = !_captureInProgress;

                if (_captureInProgress)
                {
                    buttonNext.Enabled = true;
                    buttonPrev.Enabled = true;
                    checkBoxTestImage.Enabled = true;
                    buttonDeleteImage.Enabled = true;

                    //Update the scale and offset value
                    _caculateScaleOffset();
                    
                    buttonCapture.Text = "Continue";
                    //stop the capture
                    capture.Pause();

                    //Set the image and text file name
                    int iname = 0;
                    string tmpFileName = "";
                    do
                    {
                        
                        if (iname == 0)
                        {
                            tmpFileName = imgDir + "\\" + CAPTURE_IMAGE_NAME + (allImagesFound.Count).ToString();
                        }
                        else
                        {
                            tmpFileName += "-" + iname.ToString();
                        }
                        cordTextFileName = tmpFileName + ".txt";
                        imageFileName = tmpFileName + ".jpg";
                        iname++;
                    }
                    while (allImagesFound.Contains(imageFileName));
                    
                    
                    //save the image
                    pictureBox1.Image.Save(imageFileName, System.Drawing.Imaging.ImageFormat.Jpeg);

                    allImagesFound.Add(imageFileName);
                    imgCnt = allImagesFound.Count -1;

                    _updateProcess(imgCnt);
                    
                }
                else
                {
                    buttonNext.Enabled = false;
                    buttonPrev.Enabled = false;
                    checkBoxTestImage.Checked = false;
                    checkBoxTestImage.Enabled = false;
                    //start the capture
                    buttonCapture.Text = "Capture";
                    capture.Start();
                }

            }
        }

        private void ButtonVideo_Click(object sender, EventArgs e)
        {
            if (videoFlag)
            {
                videoFlag = false;

                //Enable the Previous and Next button
                buttonNext.Enabled = true;
                buttonPrev.Enabled = true;
                checkBoxLeft.Enabled = false;
                checkBoxRight.Enabled = false;
                checkBoxStereoCamera.Enabled = false;
                checkBoxTestImage.Enabled = true;

                buttonVideo.Text = "Video Mode";
                capture.Pause();
                buttonCapture.Text = "Capture";
                buttonCapture.Enabled = false;
                buttonDeleteImage.Enabled = true;
                _captureInProgress = false;
                _loadImg(imgCnt);
                _updateProcess(imgCnt);

            }
            else 
            {
                videoFlag = true;
                buttonVideo.Text = "Image Mode";
                buttonCapture.Enabled = true;
                buttonDeleteImage.Enabled = false;
                buttonNext.Enabled = false;
                buttonPrev.Enabled = false;
                checkBoxLeft.Enabled = true;
                checkBoxRight.Enabled = true;
                checkBoxStereoCamera.Enabled = true;
                checkBoxTestImage.Enabled = false;

                //In video mode if the output dir is empty set the default to current folder    
                if (string.IsNullOrWhiteSpace(textBoxImgDir.Text))
                { 
                    textBoxImgDir.Text = Path.Combine(Directory.GetCurrentDirectory(), OUTPUT_CAPTURE_DIRECTORY);
                }

                imgDir = textBoxImgDir.Text;

                if (!Directory.Exists(imgDir))
                {
                    Directory.CreateDirectory(imgDir);
                }

                //load all existed image
                if(allImagesFound == null) _loadImgAndImageNames(false);

                //clear the listBox
                listBoxBoundary.Items.Clear();

                try
                {
                    CvInvoke.UseOpenCL = false;
                    capture = new VideoCapture(); //create a camera captue

                    capture.SetCaptureProperty(CapProp.FrameWidth, Properties.Settings.Default.CameraResolution.Width);
                    capture.SetCaptureProperty(CapProp.FrameHeight, Properties.Settings.Default.CameraResolution.Height);
                    capture.SetCaptureProperty(CapProp.Fps, Properties.Settings.Default.CameraFPS);
                    
                    capture.ImageGrabbed += ProcessFrame;
                    capture.Start();
                }
                catch (NullReferenceException excpt)
                {
                    MessageBox.Show(excpt.Message);
                    videoFlag = false;
                    buttonVideo.Text = "Video Mode";
                }
                
                _frame = new Mat();
                
            }
        }

        private void ButtonDeleteImage_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxImgDir.Text)) return;
            if (allImagesFound.Count != 0 && (_captureInProgress || !videoFlag))
            {
                listBoxBoundary.Items.Clear();
                //Delete the image and text file
                File.Delete(allImagesFound[imgCnt]);
                File.Delete(cordTextFileName);

                //remove from the list
                allImagesFound.Remove(allImagesFound[imgCnt]);

                if (imgCnt == allImagesFound.Count)
                {

                    imgCnt = imgCnt - 1;

                }
                
                //Load next image
                _loadImg(imgCnt);
                _updateProcess(imgCnt);

            }
        }

        private void CheckBoxStereoCamera_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxStereoCamera.CheckState == CheckState.Checked)
            {
                checkBoxLeft.Checked = true;
            }
            else
            {
                checkBoxLeft.Checked = false;
                checkBoxRight.Checked = false;
            }
        }

        private void ButtonGenerateList_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(imgDir))
            {
                for(int i=0;i<testList.Count;i++)
                {
                    
                    if (!File.Exists(testList[i].Replace(Path.GetExtension(testList[i]), ".txt")))
                    {
#if DEBUG
                        Console.WriteLine(Path.GetExtension(testList[i]));
                        Console.WriteLine(testList[i]);
#endif
                        testList.RemoveAt(i);
                    }
                }

                for (int i = 0; i < trainList.Count; i++)
                {
                    if (!File.Exists(trainList[i].Replace(Path.GetExtension(trainList[i]), ".txt")))
                    {
                        Console.WriteLine(trainList[i]);
                        trainList.RemoveAt(i);
                    }
                }
                //Generate the ListofImageDirectory.txt file 
                string allImgTextFilePath = Path.Combine(imgDir, All_IMAGE_FILE);
                System.IO.File.WriteAllLines(allImgTextFilePath, allImagesFound);
                //Generate the test.txt file 
                string testFilePath = Path.Combine(imgDir, TEST_LIST_FILE);
                System.IO.File.WriteAllLines(testFilePath, testList);
                //Generate the train.txt file 
                string trainFilePath = Path.Combine(imgDir, TRAIN_LIST_FILE);
                System.IO.File.WriteAllLines(trainFilePath, trainList);

                string backupDirectory = Path.Combine(imgDir, "Backup");
                if (!Directory.Exists(backupDirectory)) Directory.CreateDirectory(backupDirectory);

                //Generate the obj.data file 
                string objectData = string.Format("classes= {0}{1}", comboBoxListLabel.Items.Count,Environment.NewLine);
                objectData += string.Format("train= {0}{1}", trainFilePath, Environment.NewLine);
                objectData += string.Format("valid= {0}{1}", testFilePath, Environment.NewLine);
                objectData += string.Format("names= {0}{1}", Path.Combine(imgDir, OBJ_NAMES_FILE), Environment.NewLine);
                objectData += string.Format("backup= {0}{1}", backupDirectory, Environment.NewLine);
#if DEBUG
                Console.WriteLine(objectData);
#endif
                string objDataFilePath = Path.Combine(imgDir, OBJ_DATA_FILE);
                System.IO.File.WriteAllText(objDataFilePath, objectData);

                MessageBox.Show(@"Generating the list finished!");


            }
        }

        private void ButtonStartTraining_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(imgDir)) return;
            if(!File.Exists(Path.Combine(imgDir, TEST_LIST_FILE)))
            {
                MessageBox.Show(string.Format("{0} does not exist!{1}Please generate it", TEST_LIST_FILE,Environment.NewLine));
                return;
            }
            if (!File.Exists(Path.Combine(imgDir, OBJ_NAMES_FILE)))
            {
                MessageBox.Show(string.Format("{0} does not exist!{1}Please generate it", OBJ_NAMES_FILE, Environment.NewLine));
                return;
            }
            if (!File.Exists(Path.Combine(imgDir, TRAIN_LIST_FILE)))
            {
                MessageBox.Show(string.Format("{0} does not exist!{1}Please generate it", TRAIN_LIST_FILE, Environment.NewLine));
                return;
            }
            if (!File.Exists(Path.Combine(imgDir, OBJ_DATA_FILE)))
            {
                MessageBox.Show(string.Format("{0} does not exist!{1}Please generate it", OBJ_DATA_FILE, Environment.NewLine));
                return;
            }

            string backupDirectory = Path.Combine(imgDir, "Backup");
            if (!Directory.Exists(backupDirectory)) Directory.CreateDirectory(backupDirectory);

            Process trainingApp = new Process();
           
            trainingApp.StartInfo.FileName = Properties.Settings.Default.trainApp;
            string workSpace = Path.GetDirectoryName(Properties.Settings.Default.trainApp);
            string appName = Path.GetFileNameWithoutExtension(Properties.Settings.Default.trainApp);
            try
            {
                if (workSpace != string.Empty)
                {
                    Directory.SetCurrentDirectory(workSpace);
                }

                trainingApp.StartInfo.Arguments = Properties.Settings.Default.AppArgument;
                if(appName == "darknet")
                {
                    trainingApp.StartInfo.Arguments=trainingApp.StartInfo.Arguments.Replace("OBJECTDATAFILE", Path.Combine(imgDir, OBJ_DATA_FILE));
                }
#if DEBUG
                Console.WriteLine(trainingApp.StartInfo.FileName);
                Console.WriteLine(trainingApp.StartInfo.Arguments);
#endif
                trainingApp.Start();
            }
            catch(Exception ecpt)
            {
                string msg = string.Format(@"Trainning app setting error! {1}{0}", ecpt.ToString(),Environment.NewLine);
                MessageBox.Show(msg);
            }
        }

        private void ButtonEdit_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxImgDir.Text)) return;
            LabelConfig LabelConfigForm = new LabelConfig(imgDir);
            LabelConfigForm.ShowDialog();
            LoadLabelList();

        }

        public void LoadLabelList()
        {
            string LabelListFileName = Path.Combine(imgDir, OBJ_NAMES_FILE);
            if (File.Exists(LabelListFileName))
            {
                string[] lines = File.ReadAllLines(LabelListFileName);
                comboBoxListLabel.DataSource = lines;
                
            }
            else
            {
                MessageBox.Show("Warning! Label file does not exist!");

            }
        }

        private void checkBoxTestImage_CheckedChanged(object sender, EventArgs e)
        {
            if (videoFlag && !_captureInProgress)
            {
                checkBoxTestImage.Checked = false;
                return;
            }

            imageFileName = Path.GetFileNameWithoutExtension(allImagesFound[imgCnt]);
            string oldTextFile = imgDir + "\\" + imageFileName + ".txt";
            string oldImageFile = allImagesFound[imgCnt];

            if (checkBoxTestImage.Checked && !allImagesFound[imgCnt].Contains(TEST_MARK))
            {
                string newTextFile = imgDir + "\\" + imageFileName + TEST_MARK + ".txt";
                string newImageFile = imgDir + "\\" + imageFileName + TEST_MARK + ".jpg";
                allImagesFound[imgCnt] = newImageFile;
                trainList.Remove(oldImageFile);
                testList.Add(newImageFile);
                File.Move(oldImageFile, newImageFile);
                if(File.Exists(oldTextFile)) File.Move(oldTextFile, newTextFile);
            }
            if (!checkBoxTestImage.Checked && allImagesFound[imgCnt].Contains(TEST_MARK))
            {
                string newTextFile = oldTextFile.Replace(TEST_MARK,string.Empty);
                string newImageFile = oldImageFile.Replace(TEST_MARK, string.Empty);
                allImagesFound[imgCnt] = newImageFile;
                trainList.Add(newImageFile);
                testList.Remove(oldImageFile);
                File.Move(oldImageFile, newImageFile);
                if (File.Exists(oldTextFile)) File.Move(oldTextFile, newTextFile);
            }
        }
    }
}
